import React from 'react';
import {List, ListItem, Avatar, Subheader} from 'material-ui';
import MoodBadIcon from 'material-ui/svg-icons/social/mood-bad';
import FriendsListElement from './friendsPanel';

import styles from './css/styles.css';
import {loadFriendsSerialsStatus} from "../../actions/friends/friendsSerialsInfo";
import {STATUS_LIST} from "../../lib/utils/utils";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    friendsSerialsInfo: React.PropTypes.object,
    params: React.PropTypes.object
};

class FriendsSerialsList extends React.Component {

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(loadFriendsSerialsStatus(this.props.params['friendId']));
    }

    render() {
        var data = [];
        for (let key in this.props.friendsSerialsInfo.statuses) {
            console.log(key);
            data.push(
                <ListItem
                    key={key}
                    primaryText={STATUS_LIST[key - 1].name}
                    initiallyOpen={true}
                    primaryTogglesNestedList={true}
                    nestedItems={
                        this.props.friendsSerialsInfo.statuses[key].map(show =>
                            <ListItem
                                key={show.TVShowID}
                                primaryText={show.TVShowName}
                            />
                        )
                    }
                >

                </ListItem>
            );
        }
        return (
            <List className={styles.list_panel}>
                <h4>Shows</h4>
                {data}
            </List>
        );
    }
}

FriendsSerialsList.propTypes = propTypes;
export default FriendsSerialsList;