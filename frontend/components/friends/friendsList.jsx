import React from 'react';
import {List, ListItem, Avatar, Subheader} from 'material-ui';
import MoodBadIcon from 'material-ui/svg-icons/social/mood-bad';
import FriendsListElement from './friendsPanel';

import styles from './css/styles.css';

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    friendsList: React.PropTypes.object
};

class FriendsList extends React.Component {
    render() {
        return (
            <List className={styles.list}>
                <Subheader className={styles.title}>Your friends</Subheader>
                {
                    this.props.friendsList.list.length < 1 ?
                    <div>
                        <ListItem
                            leftAvatar={<Avatar icon={<MoodBadIcon />} />}
                            primaryText={'You don\'t have any friends yet...'}
                        />
                    </div> : 
                    this.props.friendsList.list.map((friendInfo) =>
                    <div key={friendInfo.Person.UserID}>
                        <FriendsListElement
                            dispatch={this.props.dispatch}
                            friendInfo={friendInfo}
                            removeModal={this.props.friendsList.removeModal}
                            removeModalContent={this.props.friendsList.removeModalContent}
                            removeFriendId={this.props.friendsList.removeFriendId}
                        />
                    </div>
                    )
                }                
            </List>
        );
    }
}

FriendsList.propTypes = propTypes;
export default FriendsList;