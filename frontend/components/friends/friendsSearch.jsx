import React from 'react';
import {TextField, FlatButton, List, ListItem, Avatar, IconButton} from 'material-ui';
import AccountIcon from 'material-ui/svg-icons/action/account-circle';
import AddIcon from 'material-ui/svg-icons/content/add';
import {lightBlack} from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';

import autobind from 'autobind-decorator';
import {connect} from 'react-redux';
import styles from './css/styles.css'

import {openAddModal} from "../../actions/friends/friendsSearch";
import {closeAddModal} from '../../actions/friends/friendsSearch';
import {addFriend} from '../../actions/friends/friendsSearch';
import {changeSearch} from '../../actions/friends/friendsSearch';

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    friendsSearch: React.PropTypes.object
};

class FriendsSearch extends React.Component {

    @autobind
    handleChange(event) {
        const {dispatch} = this.props;
        dispatch(changeSearch(event.target.value));
    };

    @autobind
    handleOpenAddModal() {
        const {dispatch} = this.props;
        dispatch(openAddModal(this.props.friendsSearch.searchField));
    };

    @autobind
    handleCloseAddModal() {
        const {dispatch} = this.props;
        dispatch(closeAddModal());
    };

    @autobind
    handleAddFriend() {
        console.log(this.props.friendsSearch.addFriendId);
        const {dispatch} = this.props;
        dispatch(addFriend(this.props.friendsSearch.addFriendId));
        this.handleCloseAddModal()
    };

    render() {
        const actions = [
            <FlatButton
                label="Add"
                primary={true}
                onTouchTap={this.handleAddFriend}
                disabled={!this.props.friendsSearch.findSuccess}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleCloseAddModal}
            />,
            ];

        return (
            <div className={styles.container}>
                <TextField
                    hintText="Enter friends Email"
                    className={styles.text_field}
                    onChange={this.handleChange}
                    value={this.props.friendsSearch.searchField}
                />
                <FlatButton 
                    label="Find" 
                    primary={true} 
                    onTouchTap={this.handleOpenAddModal}
                />
                <Dialog
                    title={this.props.friendsSearch.addModalTitle}
                    actions={actions}
                    modal={true}
                    open={this.props.friendsSearch.addModal}
                    >
                    {
                        this.props.friendsSearch.addModalContent
                    }
                </Dialog>
            </div>
        );
    }
}

FriendsSearch.propTypes = propTypes;
export default FriendsSearch;