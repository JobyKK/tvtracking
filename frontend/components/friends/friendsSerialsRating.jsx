import React from 'react';
import {List, ListItem, Avatar, Subheader} from 'material-ui';
import MoodBadIcon from 'material-ui/svg-icons/social/mood-bad';
import FriendsListElement from './friendsPanel';

import styles from './css/styles.css';
import {loadFriendsSerialsStatus, loadFriendsSerialsRatings} from "../../actions/friends/friendsSerialsInfo";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    friendsSerialsInfo: React.PropTypes.object,
    params: React.PropTypes.object
};

class FriendsSerialsRating extends React.Component {

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(loadFriendsSerialsRatings(this.props.params['friendId']));
    }

    render() {
        return (
        <div className={styles.list_panel}>
            <h4>Ratings</h4>
            <List>
                {
                    this.props.friendsSerialsInfo.ratings.map(rating =>
                        <ListItem
                            key={"rating" + rating.TVShowID}
                            primaryText={rating.TVShowName + ": " + rating.Rating}
                        >
                        </ListItem>
                    )
                }
            </List>
        </div>
        );
    }
}

FriendsSerialsRating.propTypes = propTypes;
export default FriendsSerialsRating;