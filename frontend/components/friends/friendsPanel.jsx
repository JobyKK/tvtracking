import React from 'react';
import {ListItem, Avatar, IconButton, FlatButton} from 'material-ui';
import AccountIcon from 'material-ui/svg-icons/action/account-circle';
import RemoveIcon from 'material-ui/svg-icons/content/clear';
import {lightBlack} from 'material-ui/styles/colors';

import Dialog from 'material-ui/Dialog';

import {browserHistory} from 'react-router';
import autobind from 'autobind-decorator';
import {connect} from 'react-redux';
import styles from './css/styles.css';

import {openRemoveModal} from "../../actions/friends/friendsList";
import {closeRemoveModal} from '../../actions/friends/friendsList';
import {removeFriend} from '../../actions/friends/friendsList';
import {friendUrl} from "../../routes";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    friendInfo: React.PropTypes.object
};

class FriendsListElement extends React.Component {

    @autobind
    handleOpenRemoveModal() {
        const {dispatch} = this.props;
        dispatch(openRemoveModal(this.props.friendInfo.Person.UserID, this.props.friendInfo.Person.Name, this.props.friendInfo.Person.Email));
    };

    @autobind
    handleCloseRemoveModal() {
        const {dispatch} = this.props;
        dispatch(closeRemoveModal());
    };

    @autobind
    handleRemoveFriend() {
        const {dispatch} = this.props;
        dispatch(removeFriend(this.props.removeFriendId));
    };

    @autobind
    openFriendsInfo() {
        browserHistory.push(friendUrl + this.props.friendInfo.Person.UserID)
    };

    render() {
        const actions = [
            <FlatButton
                label="Remove"
                primary={true}
                onTouchTap={this.handleRemoveFriend}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleCloseRemoveModal}
            />,
        ];

        return (
            <div>
                <ListItem
                    leftAvatar={
                        <div onClick={this.openFriendsInfo}>
                            <Avatar />
                        </div>
                    }
                    primaryText={this.props.friendInfo.Person.Name}
                    secondaryText={this.props.friendInfo.Person.Email}
                    rightIcon={
                        <IconButton
                            touch={true}
                            tooltip="Remove"
                            tooltipPosition="top-right"
                            className={styles.icon_button}
                            onTouchTap={this.handleOpenRemoveModal}
                        >
                            <RemoveIcon color={lightBlack}/>
                        </IconButton>
                    }
                />
                <Dialog
                    title="Remove this friend from your friends list?"
                    actions={actions}
                    modal={true}
                    open={this.props.removeModal}
                >
                    {
                        this.props.removeModalContent
                    }
                </Dialog>
            </div>
        );
    }
}

FriendsListElement.propTypes = propTypes;
export default FriendsListElement;