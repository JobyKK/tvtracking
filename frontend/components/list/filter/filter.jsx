import React from 'react';
import autobind from 'autobind-decorator';
import {AutoComplete} from 'material-ui';
import {fetchRecommendedListInput} from '../../../actions/list/filter'
import styles from './css/styles.css'
import {filterNameChanged} from "../../../actions/list/filter";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    showFilter: React.PropTypes.object,
};

class Filter extends React.Component {

    @autobind
    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(fetchRecommendedListInput())
    }

    @autobind
    handleUpdateInput(value) {
        const {dispatch} = this.props;
        dispatch(filterNameChanged(this.props.showFilter, value))
    };

    render() {
        return (
            <div className={styles.container}>
                <AutoComplete
                    className={styles.name_input}
                    hintText="TV show's name"
                    dataSource={this.props.showFilter.recommendedShowList}
                    onUpdateInput={this.handleUpdateInput}
                    filter={AutoComplete.caseInsensitiveFilter}
                    value={this.props.showFilter.name}
                />
            </div>
        );
    }
}

Filter.propTypes = propTypes;
export default Filter;