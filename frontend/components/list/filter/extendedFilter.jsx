import React from 'react';
import autobind from 'autobind-decorator'
import {RadioButtonGroup, RadioButton, List, ListItem} from "material-ui";
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
// import {verifyClientRequest, cancelVerifyClientRequest} from './../../actions/client';
import styles from './css/styles.css'
import {updateGenreFilter} from "../../../actions/list/filter";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    showFilter: React.PropTypes.object,
    genres: React.PropTypes.object
};


const buttonStyle = {};

class ExtendedFilter extends React.Component {

    @autobind
    handleGenreSelected(genreId) {
        const {dispatch} = this.props;
        dispatch(updateGenreFilter(this.props.showFilter, genreId));
    }

    render() {
        console.log(String(this.props.showFilter.genreId))
        return (
            <div className={styles.container}>
                <List>
                    <ListItem
                        primaryText="Genres"
                        initiallyOpen={true}
                        primaryTogglesNestedList={true}
                        nestedItems={[
                            <ListItem
                                key={1}
                                disabled={true}
                            >
                                <RadioButtonGroup name="genreSelect"
                                                  onChange={(ev, value) => this.handleGenreSelected(value)}
                                                  defaultSelected={String(this.props.showFilter.genreId)}
                                >
                                    {
                                        this.props.genres.list.map(genre =>
                                            <RadioButton
                                                key={"genre"+genre.id}
                                                style={buttonStyle}
                                                iconStyle={{marginRight: '5px'}}
                                                labelStyle={{marginRight: '10px'}}
                                                value={genre.id}
                                                label={genre.name}
                                            />
                                        )
                                    }

                                </RadioButtonGroup>
                            </ListItem>
                        ]}
                    />
                </List>
            </div>
        );
    }
}

ExtendedFilter.propTypes = propTypes;
export default ExtendedFilter;