import React from 'react';
import autobind from 'autobind-decorator'
import {Card, FloatingActionButton} from 'material-ui';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {verifyClientRequest, cancelVerifyClientRequest} from './../../actions/client';
import styles from './css/styles.css'
import {fetchShowsByFilter} from "../../actions/list/list";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    showInfo: React.PropTypes.object,
    showFilter: React.PropTypes.object,
    currentPage: React.PropTypes.number
};

class LoadButton extends React.Component {

    @autobind
    handleAddClickEvent() {
        const {dispatch} = this.props;
        dispatch(fetchShowsByFilter(this.props.showFilter, this.props.currentPage + 1));
    };

    render() {
        return (
            <Card className={styles.show_container}>
                <div className={styles.add_button_content}>
                    <h1>Load more</h1>
                    <FloatingActionButton onClick={this.handleAddClickEvent} className={styles.add_button}>
                        <ContentAdd />
                    </FloatingActionButton>
                </div>
            </Card>
        );
    }
}

LoadButton.propTypes = propTypes;
export default LoadButton;