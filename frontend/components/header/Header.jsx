import React from 'react';
import autobind from 'autobind-decorator';
import {connect} from 'react-redux';
import {IconMenu, IconButton, MenuItem, AppBar, FlatButton, Drawer, Divider} from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import FriendsIcon from 'material-ui/svg-icons/action/face';
import ShowsIcon from 'material-ui/svg-icons/notification/live-tv';
import CalendarIcon from 'material-ui/svg-icons/action/date-range';
import {browserHistory} from 'react-router';


import styles from './css/styles.css';
import {logoutUser} from "../../actions/auth/logout";
import {toggleDrawer} from '../../actions/header/drawer';
import {closeDrawer} from '../../actions/header/drawer';

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.object,
    drawer: React.PropTypes.object
};

class Header extends React.Component {

    static handleMenuItemClickedEvent() {
        browserHistory.push('/');
    };

    static handleMenuShowsClickedEvent() {
        browserHistory.push('/');
    };

    static handleMenuCalendarClickedEvent() {
        browserHistory.push('/');
    };

    static handleMenuFriendsClickedEvent() {
        browserHistory.push('/friends');
    };

    static handleClickLoginClientEvent() {
        browserHistory.push('/login');
    };

    @autobind
    handleToggleDrawer() {
        const {dispatch} = this.props;
        dispatch(toggleDrawer(!this.props.drawer.open));
    };

    @autobind
    handleCloseDrawer() {
        const {dispatch} = this.props;
        dispatch(closeDrawer());
    };

    @autobind
    handleLogoutClickEvent() {
        const {dispatch} = this.props;
        dispatch(logoutUser());
    };

    render() {
        return (
            <div className={styles.container}>
                <AppBar
                    className={styles.header_content}
                    title="TV Shows"
                    onTitleTouchTap={Header.handleMenuItemClickedEvent}
                    onLeftIconButtonTouchTap={this.handleToggleDrawer}
                    iconElementRight={this.props.user.isAuthenticated
                        ? <IconMenu
                            iconButtonElement={
                                <IconButton><MoreVertIcon /></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                        >
                            {/*<MenuItem primaryText="Help"/>*/}
                            <MenuItem primaryText="Logout"
                                onClick={this.handleLogoutClickEvent}
                            />
                        </IconMenu>
                        : <FlatButton
                            className={styles.login_button}
                            onClick={Header.handleClickLoginClientEvent} label="Login"
                        />
                    }
                />
                <Drawer 
                    containerClassName={this.props.drawer.open && this.props.user.isAuthenticated ? styles.drawer_menu_open : styles.drawer_menu_close}
                    open={this.props.drawer.open && this.props.user.isAuthenticated}
                >
                    <MenuItem 
                        primaryText="MyShows"
                        leftIcon={<ShowsIcon />}
                        onClick={Header.handleMenuShowsClickedEvent}
                        onTouchTap={this.handleCloseDrawer}
                    />
                    <Divider />
                    <MenuItem 
                        primaryText="Calendar"
                        leftIcon={<CalendarIcon />}
                        onClick={Header.handleMenuCalendarClickedEvent}
                        onTouchTap={this.handleCloseDrawer}
                    />
                    <Divider />
                    <MenuItem 
                        primaryText="Friends"
                        leftIcon={<FriendsIcon />}
                        onClick={Header.handleMenuFriendsClickedEvent}
                        onTouchTap={this.handleCloseDrawer}
                    />
                    <Divider />
                </Drawer>
            </div>
        );
    }
}

Header.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, drawer} = state;
    return {
        user,
        drawer
    };
}

export default connect(mapStateToProps)(Header);