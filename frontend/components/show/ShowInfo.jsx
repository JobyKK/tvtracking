import React from 'react';
import {
    Chip, Paper, Divider
} from 'material-ui';
import styles from './css/styles.css'
import {formatDate} from "../../lib/utils/utils";
import SeasonsBlock from "../../components/show/SeasonsBlock";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    show: React.PropTypes.object
};

const chipStyles = {
    general: {
        margin: 4,
    },
    flat: {
        margin: 4,
        line_height: 16
    }
};

class ShowInfo extends React.Component {
    render() {
        return (
            <Paper zDepth={1} className={styles.info_container}>
                <div className={styles.info_block}>
                    <span className={styles.flat_title}>Year: </span>
                    <span>
                        {
                            formatDate(this.props.show.date)
                        }
                    </span>
                </div>

                <Divider />

                <div className={styles.info_block}>
                    <span className={styles.label_info}>
                        <label>Genres:</label>
                    </span>

                    <div className={styles.genre_list}>
                        {
                            this.props.show.genres.map((genre) =>
                                <Chip
                                    key={genre.id}
                                    style={chipStyles.general}
                                      className={styles.list_element}>
                                    {genre.name}
                                </Chip>
                            )
                        }
                    </div>
                </div>

                <Divider />

                <div className={styles.info_block}>
                    <span className={styles.label_info}>Country: </span>
                    <span>
                        {
                            this.props.show.country.map((country) =>
                                <Chip
                                    key={country}
                                    style={chipStyles.flat}
                                      className={styles.list_element}>
                                    {country}
                                </Chip>
                            )
                        }
                    </span>
                </div>

                <Divider />

                <div className={styles.info_block}>
                    <span>
                        {
                            this.props.show.overview
                        }
                    </span>
                </div>

                <Divider />

                <div className={styles.info_block}>
                    <SeasonsBlock {...this.props}/>
                </div>

            </Paper>
        );
    }
}

ShowInfo.propTypes = propTypes;
export default ShowInfo;