import React from 'react';
import autobind from "autobind-decorator";
import {
    List, ListItem, Checkbox
} from 'material-ui';
import {generateUniqueNumber} from "../../lib/utils/generator";
import {updateEpisodeStatus} from "../../actions/show/show";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    show: React.PropTypes.object
};

class SeasonsBlock extends React.Component {

    @autobind
    handleCheckEpisode(event, isChecked, season_id, episode_id) {
        const {dispatch} = this.props;
        dispatch(updateEpisodeStatus(this.props.show.id, isChecked, season_id, episode_id))
    }

    render() {
        return (
            <List>
                {
                    this.props.show.seasons.map((season) => <ListItem
                            key={season.season_number}
                            primaryText={season.name}
                            initiallyOpen={false}
                            primaryTogglesNestedList={true}
                            nestedItems={
                                season.episodes.map((episode) => {
                                        let key = generateUniqueNumber(
                                            season.season_number,
                                            episode.episode_number
                                        );
                                        let episodeName = "Episode " + (episode.episode_number)
                                            + ": " + episode.name;

                                        return this.props.user.isAuthenticated ?
                                            <ListItem
                                                key={Math.random()}
                                                primaryText={episodeName}
                                                leftCheckbox={
                                                    <Checkbox
                                                        onCheck={(e, checked) => this.handleCheckEpisode(
                                                            e, checked,
                                                            season.season_number,
                                                            episode.episode_number)}
                                                        checked={episode.userStatus}
                                                    />
                                                }
                                            />
                                            :
                                            <ListItem
                                                key={key}
                                                primaryText={episodeName}
                                            />
                                    }
                                )}
                        />
                    )
                }

            </List>
        );
    }
}

SeasonsBlock.propTypes = propTypes;
export default SeasonsBlock;