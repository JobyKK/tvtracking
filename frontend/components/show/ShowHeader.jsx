import React from "react";
import autobind from "autobind-decorator";
import {DropDownMenu, MenuItem} from "material-ui";
import styles from "./css/styles.css";
import {SHOW_STATUS_TYPE} from "../../lib/utils/utils";
import {updateShowStatus} from "../../actions/show/show";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    show: React.PropTypes.object
};

class ShowHeader extends React.Component {

    @autobind
    handleStatusChanged(event, index, value) {
        const {dispatch} = this.props;
        dispatch(updateShowStatus(this.props.show.id, value, this.props.show.name))
    }

    render() {
        return (
            <div className={styles.header_container}>
                <div className={styles.header_name}>
                    {this.props.show.name}
                </div>

                {
                    this.props.user.isAuthenticated &&
                    <div className={styles.header_status}>
                    <span className={styles.header_status_label}>
                        Status:
                    </span>

                        <DropDownMenu
                            value={this.props.show.userStatus}
                            onChange={this.handleStatusChanged}>
                            <MenuItem value={SHOW_STATUS_TYPE.UNSEEN.id}
                                      primaryText={SHOW_STATUS_TYPE.UNSEEN.name}/>
                            <MenuItem value={SHOW_STATUS_TYPE.IN_PROGRESS.id}
                                      primaryText={SHOW_STATUS_TYPE.IN_PROGRESS.name}/>
                            <MenuItem value={SHOW_STATUS_TYPE.ON_HOLD.id}
                                      primaryText={SHOW_STATUS_TYPE.ON_HOLD.name}/>
                            <MenuItem value={SHOW_STATUS_TYPE.SEEN.id}
                                      primaryText={SHOW_STATUS_TYPE.SEEN.name}/>
                            <MenuItem value={SHOW_STATUS_TYPE.WANT_TO_WATCH.id}
                                      primaryText={SHOW_STATUS_TYPE.WANT_TO_WATCH.name}/>
                        </DropDownMenu>
                    </div>
                }
            </div>
        );
    }
}

ShowHeader.propTypes = propTypes;
export default ShowHeader;