import React from "react";
import autobind from "autobind-decorator";
import {RadioButtonGroup, RadioButton} from "material-ui";
import styles from "./css/styles.css";
import {SHOW_STATUS_TYPE} from "../../lib/utils/utils";
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import {updateEpisodeStatus, updateShowRate} from "../../actions/show/show";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    show: React.PropTypes.object
};

const buttonStyle = {
    display: "block",
    float: "left",
    width: 'auto'
};

export class ShowRate extends React.Component {

    @autobind
    handleRateChanged(value) {
        const {dispatch} = this.props;
        dispatch(updateShowRate(this.props.show.id, value, this.props.show.name))
    }

    render() {
        return (
            <div className={styles.rate_container}>
                <RadioButtonGroup name="showRate"
                                  onChange={(ev, value) => this.handleRateChanged(value)}
                                  defaultSelected={String(this.props.show.userRating)}>
                    <RadioButton
                        style={buttonStyle}
                        iconStyle={{ marginRight: '5px' }}
                        labelStyle={{ marginRight: '10px' }}
                        className={styles.rate_button}
                        value="1"
                        label="1"
                        checkedIcon={<ActionFavorite style={{color: '#F44336'}}/>}
                        uncheckedIcon={<ActionFavoriteBorder />}
                    />
                    <RadioButton
                        style={buttonStyle}
                        iconStyle={{ marginRight: '5px' }}
                        labelStyle={{ marginRight: '10px' }}
                        className={styles.rate_button}
                        value="2"
                        label="2"
                        checkedIcon={<ActionFavorite style={{color: '#F44336'}}/>}
                        uncheckedIcon={<ActionFavoriteBorder />}
                    />
                    <RadioButton
                        style={buttonStyle}
                        iconStyle={{ marginRight: '5px' }}
                        labelStyle={{ marginRight: '10px' }}
                        className={styles.rate_button}
                        value="3"
                        label="3"
                        checkedIcon={<ActionFavorite style={{color: '#F44336'}}/>}
                        uncheckedIcon={<ActionFavoriteBorder />}
                    />
                    <RadioButton
                        style={buttonStyle}
                        iconStyle={{ marginRight: '5px' }}
                        labelStyle={{ marginRight: '10px' }}
                        className={styles.rate_button}
                        value="4"
                        label="4"
                        checkedIcon={<ActionFavorite style={{color: '#F44336'}}/>}
                        uncheckedIcon={<ActionFavoriteBorder />}
                    />
                    <RadioButton
                        style={buttonStyle}
                        iconStyle={{ marginRight: '5px' }}
                        labelStyle={{ marginRight: '10px' }}
                        className={styles.rate_button}
                        value="5"
                        label="5"
                        checkedIcon={<ActionFavorite style={{color: '#F44336'}}/>}
                        uncheckedIcon={<ActionFavoriteBorder />}
                    />
                </RadioButtonGroup>
            </div>
        );
    }
}

ShowRate.propTypes = propTypes;
export default ShowRate;