const FILTER_TYPE = {
    STARTING_WITH: "startingWith",
    MIN_WIDTH: "minWidth"
};

const startingWithFilter = (value, subString) => {
    return value.startsWith(subString)
};

const minWidthFilter = (value, minWidth) => {
    return value.length > minWidth;
};

function customStringFilter(filterType) {
    switch (filterType) {
        case FILTER_TYPE.MIN_WIDTH:
            return minWidthFilter;
        case FILTER_TYPE.STARTING_WITH:
            return startingWithFilter;
    }
}

export function applyFilter(list, filter) {
    Object.keys(filter).forEach((filterType) => {
        list = list.filter((value) => {
            return customStringFilter(filterType)(value, filter[filterType])
        })
    });
    return list;
}

export const LIST_FILTER_TYPE = {
    BY_NAME: "Sorted by name",
    BY_GENRE: "Sorted by genre",
    RECOMMENDED: "Recommended serials"
};