const id = 'tvshow';
const USER_LOCAL_STORAGE_PATH = id + '_user';

export function saveUserLocalStorage(user) {
    localStorage.setItem(USER_LOCAL_STORAGE_PATH, JSON.stringify(user))
}

export function removeUserLocalStorage() {
    localStorage.removeItem(USER_LOCAL_STORAGE_PATH)
}

export function loadUserLocalStorage() {
    let user = localStorage.getItem(USER_LOCAL_STORAGE_PATH);
    if (user === 'undefined' || !user) return;
    return JSON.parse(user);
}