import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from 'containers/index/index';
import LoginPanel from 'containers/auth/LoginPanel';
import SignUpPanel from 'containers/auth/SignupPanel';
import ShowListContainer from './containers/list/ShowListConstainer';
import ShowContainer from './containers/show/ShowContainer';
import FriendsContainer from './containers/friends/FriendsContainer';
import FriendsSerialsContainer from './containers/friends/FriendsSerialsContainer';

export const showUrl = 'show/';
export const friendUrl = 'friend/';

export default (
    <Route name="app" component={ App } path="/">
        <IndexRoute component={ ShowListContainer }/>
        <Route path={showUrl + ":showId"} component={ ShowContainer }/>
        <Route path="login" component={ LoginPanel }/>
        <Route path="signup" component={ SignUpPanel }/>
        <Route path="friends" component={ FriendsContainer }/>
        <Route path={friendUrl + ":friendId"} component={ FriendsSerialsContainer }/>
    </Route>
);
