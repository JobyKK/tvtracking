import {checkHttpStatus, parseJSON} from "../lib/utils/utils";
export function loginUserMock(email, password) {
    return new Promise((resolve, reject) => {
        resolve(JSON.stringify(
            {
                user: {
                    email: 'ivan@gmail.com',
                    name: 'Ivan'
                }
            }
        ))
    })
}

export function loginUserRemote(email, password) {
    return fetch('/api/user/login', {
        method: 'post',
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email, password})
    })
        .then(checkHttpStatus)
        .then((response) => response.text())
        .then(parseJSON)
}

export function signUpUserMock(name, email, password) {
    return new Promise((resolve, reject) => {
        resolve(JSON.stringify(
            {
                user: {
                    email: 'ivan@gmail.com',
                    name: 'Ivan'
                }
            }
        ))
    })
}

export function signUpUserRemote(name, email, password) {
    return fetch('/api/user/create', {
        method: 'post',
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({name, email, password})
    })
    // .then(checkHttpStatus)
        .then((response) => response.text())
        .then(parseJSON)
}


export function logoutUserMock() {
    return new Promise((resolve, reject) => {
        resolve(JSON.stringify({}))
    })
}

export function logoutUserRemote() {
    return fetch('/api/user/logout', {
        method: 'post',
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    })
        .then(checkHttpStatus)
        .then((response) => response.text())
        .then(parseJSON)
}

