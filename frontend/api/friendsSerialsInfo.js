import {checkHttpStatus, parseJSON} from "../lib/utils/utils";

export function fetchFriendsSerialsStatuses(friendId) {
    return fetch('/api/friend/tvlist/' + friendId, {
        method: 'get',
        credentials: "same-origin"
    })
        .then((response) => response.text())
        .then(parseJSON)
}


export function fetchFriendsSerialsRatings(friendId) {
    return fetch('/api/friend/tvrating/' + friendId, {
        method: 'get',
        credentials: "same-origin"
    })
        .then((response) => response.text())
        .then(parseJSON)
}