import {checkHttpStatus, parseJSON} from "../lib/utils/utils";

// Friends list

export function fetchFriendsList() {
    return fetch('/api/follow/friends', {
        method: 'get',
        credentials: "same-origin"
    })
        .then((response) => response.text())
        .then(parseJSON)
}

// Remove friends

export function removeFriendFromList(person_id) {
    return fetch('/api/follow/delete', {
        method: 'delete',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({person_id}),
        credentials: "same-origin"
    })
        .then((response) => response.text())
        .then(parseJSON)
}

// Search friends

export function fetchFriendByEmail(email) {
    return fetch('/api/user/search/' + email, {
        method: 'get',
        credentials: "same-origin"
    })
        .then((response) => response.text())
        .then(parseJSON)
}

// Add friends

export function addFriendToList(person_id) {
    return fetch('/api/follow/add', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({person_id}),
        credentials: "same-origin"
    })
        .then((response) => response.text())
        .then(parseJSON)
}