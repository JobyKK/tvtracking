import {checkHttpStatus, parseJSON} from "../lib/utils/utils";
export function fetchTopShowNames(number) {
    return fetch('/api/tv/top/' + number, {
        method: 'get'
    })
        // .then(checkHttpStatus)
        .then((response) => response.text())
        .then(parseJSON)
}