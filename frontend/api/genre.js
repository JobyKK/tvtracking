import {checkHttpStatus, parseJSON} from "../lib/utils/utils";

export function fetchGenreList() {
    return fetch('/api/genre/list', {
        method: 'get'
    })
    // .then(checkHttpStatus)
        .then((response) => response.text())
        .then(parseJSON)
}
