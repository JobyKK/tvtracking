import {LIST_FILTER_TYPE} from "../lib/utils/filter";
export class ShowList {
    constructor(initialState) {
        this.isLoading = false;
        this.list = [];
        this.page = 1;
        this.filterType = LIST_FILTER_TYPE.RECOMMENDED;
        Object.assign(this, initialState);
    }
}