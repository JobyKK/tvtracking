import {SHOW_STATUS_TYPE} from '../lib/utils/utils';
/**
 * Created by andrew on 18.04.17.
 */
export class Show {
    constructor(initialState) {
        this.isLoading = false;
        Object.assign(this, initialState);
        this.userStatus = SHOW_STATUS_TYPE.UNSEEN.id;
    }
}