export class Client {
    constructor(initialState) {
        this.isVerified = false;
        this.email = '';
        this.name = '';
        Object.assign(this, initialState);
    }
}
