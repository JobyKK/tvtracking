import {USER_TYPE} from './../lib/utils/utils';
import {loadUserLocalStorage} from "../lib/utils/localStorage";

export class User {
    constructor(initialState) {
        // React.PropTypes.oneOf(['Anonymous', 'Client'])
        this.type = USER_TYPE.ANONYMOUS;
        this.isAuthenticated = false;
        Object.assign(this, initialState);
        let user = loadUserLocalStorage();
        if (user) {
            Object.assign(this, user);
            this.isAuthenticated = true;
        }
    }
}

export class LoginForm {
    constructor(initialState) {
        this.serverError = {};
    }
}

export class SignUpForm {
    constructor(initialState) {
        this.serverError = {};
    }
}