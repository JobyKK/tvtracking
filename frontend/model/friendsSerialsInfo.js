export class FriendsSerialsInfo {
    constructor(initialState) {
        this.isLoading = false;
        this.statuses = [];
        this.ratings = [];

        Object.assign(this, initialState);
    }
}