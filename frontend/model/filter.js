export class Filter {
    constructor(initialState) {
        this.isLoading = false;
        this.name = '';
        this.genreId = '';
        this.recommendedShowList = [];
        Object.assign(this, initialState);
    }
}

export class ExtendedFilter extends Filter{
    constructor(initialState) {
        super(initialState);
        this.tags = '';
        Object.assign(this, initialState);
    }
}