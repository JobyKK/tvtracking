export class FriendsList {
    constructor(initialState) {
        this.isLoading = false;
        this.list = [];

        this.removeModal = false;
        this.removeModalContent = '';
        this.removeFriendId;
        
        Object.assign(this, initialState);
    }
}