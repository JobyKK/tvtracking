export class FriendsSearch {
    constructor(initialState) {
        this.isLoading = false;

        this.searchField = '';
        this.findSuccess = false;

        this.addModal = false;
        this.addModalTitle = '';
        this.addModalContent = '';
        this.addFriendId;
        
        Object.assign(this, initialState);
    }
}