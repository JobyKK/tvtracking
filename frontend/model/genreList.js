export class Genres {
    constructor(initialState) {
        this.isLoading = false;
        this.list = [];
        Object.assign(this, initialState);
    }
}