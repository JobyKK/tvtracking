import React from "react";
import {fetchShowsByFilter} from "../../actions/list/list";
import ShowList from "./../../components/list/list";
import Filter from "./../../components/list/filter/filter";
import ExtendedFilter from "./../../components/list/filter/extendedFilter";
import {connect} from "react-redux";
import styles from "./css/styles.css";

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.object,
    showList: React.PropTypes.object,
    showFilter: React.PropTypes.object,
};


class ShowListContainer extends React.Component {

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(fetchShowsByFilter(this.props.showFilter))
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.filter_container}>
                    <Filter {...this.props} />
                    {
                        this.props.user.isAuthenticated &&
                        <ExtendedFilter {...this.props} />
                    }
                </div>
                <ShowList {...this.props} />
            </div>
        );
    }
}

ShowListContainer.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, genres, showList, showFilter} = state;
    return {
        user,
        showList,
        showFilter,
        genres
    };
}

export default connect(mapStateToProps)(ShowListContainer);