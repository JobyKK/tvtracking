import React from 'react';
import autobind from 'autobind-decorator';
import {Card, CardText, TextField, RaisedButton} from 'material-ui';
import {Link} from 'react-router';
import {signUpUser} from "../../actions/auth/signup";
import {connect} from 'react-redux';
import styles from './css/styles.css'

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
    email: React.PropTypes.string,
    password: React.PropTypes.string,
    emails: React.PropTypes.object
};


class SignUpPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: ''
        }
    }

    @autobind
    handleClickSignUpClientEvent() {
        const {dispatch} = this.props;
        dispatch(signUpUser(
            this.state.name,
            this.state.email,
            this.state.password
        ));
    }

    handleEmailChangedEvent = (event) => {
        this.setState({
            email: event.target.value
        });
    };

    handlePasswordChangedEvent = (event) => {
        this.setState({
            password: event.target.value
        });
    };

    handleNameChangedEvent = (event) => {
        this.setState({
            name: event.target.value
        });
    };

    render() {
        return (
            <Card className={styles.login_container}>
                <h2 className={styles.card_heading}>Sign Up</h2>

                {
                    this.props.signUpForm.serverError &&
                    <p className={styles.error_message}>
                        {this.props.signUpForm.serverError.message}
                    </p>
                }

                <div>
                    <TextField
                        floatingLabelText="Name"
                        name="name"
                        onChange={this.handleNameChangedEvent}
                        value={this.state.name}
                    />
                </div>

                <div>
                    <TextField
                        floatingLabelText="Email"
                        name="email"
                        onChange={this.handleEmailChangedEvent}
                        value={this.state.email}
                    />
                </div>

                <div>
                    <TextField
                        floatingLabelText="Password"
                        type="password"
                        name="password"
                        onChange={this.handlePasswordChangedEvent}
                        value={this.state.password}
                    />
                </div>

                <div className={styles.button_line}>
                    <RaisedButton type="submit"
                                  label="Create New Account" primary
                                  onClick={this.handleClickSignUpClientEvent}/>
                </div>

                <CardText>Already have an account? <Link to={'/login'}>Log in</Link></CardText>
            </Card>
        );
    }
}

SignUpPanel.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, signUpForm} = state;
    return {
        user,
        signUpForm
    };
}

export default connect(mapStateToProps)(SignUpPanel);