import React from 'react';
import autobind from 'autobind-decorator';
import {Card, CardText, TextField, RaisedButton} from 'material-ui';
import {Link} from 'react-router';
import {loginUser} from "../../actions/auth/login";
import {connect} from 'react-redux';
import styles from './css/styles.css'

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
    email: React.PropTypes.string,
    password: React.PropTypes.string,
    emails: React.PropTypes.object,
    loginForm: React.PropTypes.object
};


class LoginPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    @autobind
    handleClickLoginClientEvent() {
        const {dispatch} = this.props;
        dispatch(loginUser(
            this.state.email,
            this.state.password
        ));
    }

    handleEmailChangedEvent = (event) => {
        this.setState({
            email: event.target.value
        });
    };

    handlePasswordChangedEvent = (event) => {
        this.setState({
            password: event.target.value
        });
    };

    render() {
        return (
            <Card className={styles.login_container}>
                <h2 className={styles.card_heading}>
                    Login
                </h2>

                {
                    this.props.loginForm.serverError &&
                    <p className={styles.error_message}>
                        {this.props.loginForm.serverError.message}
                    </p>
                }

                {/*errorText={this.props.errors.email}*/}

                <div>
                    <TextField
                        floatingLabelText="Email"
                        onChange={this.handleEmailChangedEvent}
                        value={this.state.email}
                    />
                </div>
                {/*errorText={this.props.errors.password}*/}
                <div>
                    <TextField
                        floatingLabelText="Password"
                        type="password"
                        onChange={this.handlePasswordChangedEvent}
                        value={this.state.password}
                    />
                </div>
                <div className={styles.button_line}>
                    <RaisedButton
                        type="submit"
                        label="Log in" primary
                        onClick={this.handleClickLoginClientEvent}/>
                </div>

                <CardText>Don't have an account? <Link to={'/signup'}>Create one</Link>.</CardText>

            </Card>
        );
    }
}

LoginPanel.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, loginForm} = state;
    return {
        user,
        loginForm
    };
}

export default connect(mapStateToProps)(LoginPanel);