import React from 'react';
import FriendsSearch from './../../components/friends/friendsSearch';
import FriendsSerialsList from './../../components/friends/friendsSerialsList';
import FriendsSerialsRating from './../../components/friends/friendsSerialsRating';

import {connect} from 'react-redux';
import styles from './css/styles.css'

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.object,
    friendsList: React.PropTypes.object,
    friendsSearch: React.PropTypes.object
};

class FriendsSerialsContainer extends React.Component {
    render() {
        return (
            <div className={styles.container}>

                <FriendsSerialsList {...this.props} />
                <FriendsSerialsRating {...this.props} />
            </div>
        );
    }
}

FriendsSerialsContainer.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, friendsSerialsInfo} = state;
    return {
        user,
        friendsSerialsInfo
    };
}

export default connect(mapStateToProps)(FriendsSerialsContainer);