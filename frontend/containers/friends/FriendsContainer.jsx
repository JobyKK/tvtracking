import React from 'react';
import FriendsSearch from './../../components/friends/friendsSearch';
import FriendsList from './../../components/friends/friendsList';
import {loadFriendsList} from "../../actions/friends/friendsList";

import {connect} from 'react-redux';
import styles from './css/styles.css'

const propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.object,
    friendsList: React.PropTypes.object,
    friendsSearch: React.PropTypes.object
};

class FriendsContainer extends React.Component {

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(loadFriendsList());        
    }

    render() {
        return (
            <div className={styles.container}>
                <FriendsSearch {...this.props} />
                <FriendsList {...this.props} />
            </div>
        );
    }
}

FriendsContainer.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, friendsList, friendsSearch} = state;
    return {
        user,
        friendsList,
        friendsSearch
    };
}

export default connect(mapStateToProps)(FriendsContainer);