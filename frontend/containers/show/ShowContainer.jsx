import React from "react";
import {Card, CardHeader, CardMedia, CardActions, DropDownMenu, MenuItem, FlatButton} from "material-ui";
import {connect} from "react-redux";
import styles from "./css/styles.css";
import {loadShowById} from "../../actions/show/show";
import ShowInfo from "../../components/show/ShowInfo";
import ShowRate from "../../components/show/ShowRate";
import ShowHeader from "../../components/show/ShowHeader";

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.object,
    show: React.PropTypes.object,
    params: React.PropTypes.object
};

class ShowContainer extends React.Component {

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(loadShowById(this.props.params['showId']))
    }

    render() {
        return (
            <div className={styles.container}>
                {
                    this.props.show.id &&
                    <Card className={styles.show_container}>
                        <CardHeader>
                            <ShowHeader {...this.props}/>
                        </CardHeader>

                        {/*overlay={<CardTitle title="" subtitle="" />}*/}
                        <div className={styles.show_info_container}>
                            <div className={styles.image_wrapper}>
                                {
                                    this.props.show.poster &&
                                    <CardMedia>
                                        <img src={this.props.show.poster} className={styles.panel_image}/>
                                    </CardMedia>
                                }
                                {
                                    this.props.user.isAuthenticated &&
                                        <div><ShowRate {...this.props} /></div>

                                }
                            </div>

                            <ShowInfo {...this.props} />

                        </div>
                    </Card>
                }
            </div>
        );
    }
}

ShowContainer.propTypes = propTypes;

function mapStateToProps(state) {
    const {user, show, genreList} = state;
    return {
        user,
        show,
        genreList
    };
}

export default connect(mapStateToProps)(ShowContainer);