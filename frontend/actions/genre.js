import {fetchGenreList} from "../api/genre";

export function genreListRequested() {
    return {
        type: 'GENRE_LIST_REQUESTED'
    }
}

export function genreListRequestSuccess(list) {
    return {
        type: 'GENRE_LIST_REQUEST_SUCCESS',
        list
    }
}

export function genreListRequestFailure() {
    return {
        type: 'GENRE_LIST_REQUEST_FAILURE'
    }
}

export function loadGenreList() {
    return (dispatch) => {
        dispatch(genreListRequested());
        return fetchGenreList()
            .then(response => {
                // debugger
                dispatch(genreListRequestSuccess(response))
            })
            .catch((error) => {
                dispatch(genreListRequestFailure(error))
            })
    }
}