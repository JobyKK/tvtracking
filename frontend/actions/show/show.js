import {fetchShowsById, updateShowStatusById, updateEpisodeAddById, updateEpisodeDeleteById, updateShowRateById} from "../../api/show";
export function showRequested() {
    return {
        type: 'SHOW_REQUESTED'
    }
}

export function showRequestSuccess(show) {
    return {
        type: 'SHOW_REQUEST_SUCCESS',
        show
    }
}

export function showRequestFailure() {
    return {
        type: 'SHOW_REQUEST_FAILURE'
    }
}

export function updateShowStatusRequest() {
    return {
        type: 'UPDATE_SHOW_STATUS_REQUESTED'
    }
}

export function updateShowStatusSuccess(status) {
    return {
        type: 'UPDATE_SHOW_STATUS_SUCCESS',
        status
    }
}

export function updateShowStatusFailure() {
    return {
        type: 'UPDATE_SHOW_STATUS_FAILURE'
    }
}

export function updateShowRateSuccess(rating) {
    return {
        type: 'UPDATE_SHOW_RATE_SUCCESS',
        rating
    }
}

export function updateShowRateFailure() {
    return {
        type: 'UPDATE_SHOW_RATE_FAILURE'
    }
}


export function updateEpisodeStatusRequest() {
    return {
        type: 'UPDATE_EPISODE_STATUS_REQUESTED'
    }
}

export function updateEpisodeStatusSuccess(status, season, episode) {
    return {
        type: 'UPDATE_EPISODE_STATUS_SUCCESS',
        status, season, episode
    }
}

export function updateEpisodeStatusFailure() {
    return {
        type: 'UPDATE_EPISODE_STATUS_FAILURE'
    }
}


export function loadShowById(id) {
    return (dispatch) => {
        dispatch(showRequested());
        return fetchShowsById(id)
            .then(response => {
                dispatch(showRequestSuccess(response))
            })
            .catch((error) => {
                dispatch(showRequestFailure(error))
            })
    }
}


export function updateShowStatus(id, status, name) {
    return (dispatch) => {
        dispatch(updateShowStatusRequest());
        return updateShowStatusById(id, status, name)
            .then(response => {
                dispatch(updateShowStatusSuccess(status))
            })
            .catch((error) => {
                dispatch(updateShowStatusFailure(error))
            })
    }
}


export function updateShowRate(id, rate, name) {
    return (dispatch) => {
        return updateShowRateById(id, rate, name)
            .then(response => {
                dispatch(updateShowRateSuccess(rate))
            })
            .catch((error) => {
                dispatch(updateShowRateFailure(error))
            })
    }
}


export function updateEpisodeStatus(show, status, season, episode) {
    console.log({show, status, season, episode});
    return (dispatch) => {
        dispatch(updateEpisodeStatusRequest());
        let request = status ? updateEpisodeAddById: updateEpisodeDeleteById;
        return request(show, season, episode)
            .then(response => {
                dispatch(updateEpisodeStatusSuccess(status, season, episode))
            })
            .catch((error) => {
                dispatch(updateEpisodeStatusFailure(error))
            })
    }
}

