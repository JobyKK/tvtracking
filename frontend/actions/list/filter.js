import {applyFilter} from './../../lib/utils/filter';
import {fetchTopShowNames} from "../../api/filter";
import {checkHttpStatus, parseJSON} from "../../lib/utils/utils";
import {fetchShowsByFilter} from "./list";


export function fetchFilter() {
    return {
        type: 'FETCH_FILTER',
    }
}

export function fetchDefaultFilter() {
    return {
        type: 'FETCH_FILTER',
    }
}

export function showsByNameRequested() {
    return {
        type: 'SHOWS_BY_NAME_REQUESTED'
    }
}

export function showsByNameFailed() {
    return {
        type: 'SHOWS_BY_NAME_FAILED'
    }
}

export function showsByNameSuccess(list) {
    return {
        type: 'SHOWS_BY_NAME_SUCCESS',
        list
    }
}

export function filterNameChanged(filter, name) {
    return (dispatch) => {
        dispatch({
            type: 'FILTER_NAME_CHANGED',
            name
        });
        dispatch(fetchShowsByFilter({
            name
        }));
    }
}


export function updateGenreFilter(filter, genreId) {
    return (dispatch) => {
        dispatch({
            type: 'FILTER_GENRE_CHANGED',
            genreId
        });
        dispatch(fetchShowsByFilter({
            genreId
        }));
    }
}

export function recommendedListInputRequested() {
    return {
        type: 'RECOMMENDED_LIST_INPUT_REQUESTED'
    }
}

export function recommendedListInputFailed() {
    return {
        type: 'RECOMMENDED_LIST_INPUT_FAILED'
    }
}

export function recommendedListInputSuccess(list) {
    return {
        type: 'UPDATE_RECOMMENDED_LIST_INPUT_SUCCESS',
        list
    }
}

export function filterRecommendedList(list, filter) {
    if (filter) {
        list = applyFilter(list, filter);
    }
    return list;
}


export function fetchRecommendedListInput(typedValue) {
    // let filter = {
    //     startingWith: typedValue,
    //     minWidth: 1
    // };
    return function (dispatch) {
        dispatch(recommendedListInputRequested());
        return fetchTopShowNames(100)
            .then(response => {
                // response = filterRecommendedList(response, filter);
                response = response.map((show) => {
                    return show.name;
                });
                dispatch(recommendedListInputSuccess(response));
            }).catch(error => {
                recommendedListInputFailed(error)
            });
    }
}