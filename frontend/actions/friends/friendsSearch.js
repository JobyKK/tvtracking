import {fetchFriendByEmail} from "../../api/friends";
import {addFriendToList} from "../../api/friends";

export function changeSearch(value) {
    return (dispatch) => {
        return dispatch({ type: 'CHANGE_SEARCH', value });
    };
}

// Search friends

export function searchFriendRequested() {
    return {
        type: 'SEARCH_FRIEND_REQUESTED'
    }
}

export function searchFriendRequestSuccess(friend) {
    return {
        type: 'SEARCH_FRIEND_REQUEST_SUCCESS',
        friend
    }
}

export function searchFriendRequestFailure(error) {
    return {
        type: 'SEARCH_FRIEND_REQUEST_FAILURE',
        error
    }
}

export function openAddModal(email) {
    return (dispatch) => {
        dispatch(searchFriendRequested());
        return fetchFriendByEmail(email)
            .then(response => {
                dispatch(searchFriendRequestSuccess(response))
            })
            .catch((error) => {
                dispatch(searchFriendRequestFailure(error))
            })
    }
}

export function closeAddModal() {
    return (dispatch) => {
        return dispatch({ type: 'CLOSE_ADD_MODAL' });
    };
}

// Add friends

export function addFriendRequested() {
    return {
        type: 'ADD_FRIEND_REQUESTED'
    }
}

export function addFriendRequestSuccess(friend) {
    return {
        type: 'ADD_FRIEND_REQUEST_SUCCESS',
        friend
    }
}

export function addFriendRequestFailure(error) {
    return {
        type: 'ADD_FRIEND_REQUEST_FAILURE',
        error
    }
}

export function addFriend(id) {
    return (dispatch) => {
        dispatch(addFriendRequested());
        return addFriendToList(id)
            .then(response => {
                dispatch(addFriendRequestSuccess(response))
            })
            .catch((error) => {
                dispatch(addFriendRequestFailure(error))
            })
    }
}
