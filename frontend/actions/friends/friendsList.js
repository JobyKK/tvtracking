import {fetchFriendsList} from "../../api/friends";
import {removeFriendFromList} from "../../api/friends";

// Friends list

export function friendsListRequested() {
    return {
        type: 'FRIENDS_LIST_REQUESTED'
    }
}

export function friendsListRequestSuccess(list) {
    return {
        type: 'FRIENDS_LIST_REQUEST_SUCCESS',
        list
    }
}

export function friendsListRequestFailure(error) {
    return {
        type: 'FRIENDS_LIST_REQUEST_FAILURE',
        error
    }
}

export function loadFriendsList() {
    return (dispatch) => {
        dispatch(friendsListRequested());
        return fetchFriendsList()
            .then(response => {
                dispatch(friendsListRequestSuccess(response))
            })
            .catch((error) => {
                dispatch(friendsListRequestFailure(error))
            })
    }
}

// Remove Modal

export function openRemoveModal(id, name, email) {
    return (dispatch) => {
        return dispatch({ type: 'OPEN_REMOVE_MODAL', id, name, email });
    };
}

export function closeRemoveModal() {
    return (dispatch) => {
        return dispatch({ type: 'CLOSE_REMOVE_MODAL' });
    };
}

// Remove friends

export function removeFriendRequested() {
    return {
        type: 'REMOVE_FRIEND_REQUESTED'
    }
}

export function removeFriendRequestSuccess(friend) {
    return {
        type: 'REMOVE_FRIEND_REQUEST_SUCCESS',
        friend
    }
}

export function removeFriendRequestFailure(error) {
    return {
        type: 'REMOVE_FRIEND_REQUEST_FAILURE',
        error
    }
}

export function removeFriend(id) {
    return (dispatch) => {
        dispatch(removeFriendRequested());
        return removeFriendFromList(id)
            .then(response => {
                dispatch(removeFriendRequestSuccess(response))
            })
            .catch((error) => {
                dispatch(removeFriendRequestFailure(error))
            })
    }
}

