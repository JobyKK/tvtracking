import {fetchFriendByEmail} from "../../api/friends";
import {addFriendToList} from "../../api/friends";
import {fetchFriendsSerialsStatus, fetchFriendsSerialsStatuses, fetchFriendsSerialsRatings} from "../../api/friendsSerialsInfo";

export function fetchFriendsSerialsStatusRequested() {
    return {
        type: 'FETCH_FRIENDS_SERIALSREQUESTED'
    }
}

export function fetchFriendsSerialsStatusSuccess(list) {
    return {
        type: 'FETCH_FRIENDS_SERIALS_REQUEST_SUCCESS',
        list
    }
}

export function fetchFriendsSerialsStatusFailure(error) {
    return {
        type: 'FETCH_FRIENDS_SERIALS_REQUEST_FAILURE',
        error
    }
}


export function fetchFriendsSerialsRatingsRequested() {
    return {
        type: 'FETCH_FRIENDS_RATINGS_REQUESTED'
    }
}

export function fetchFriendsSerialsRatingsSuccess(list) {
    return {
        type: 'FETCH_FRIENDS_RATINGS_REQUEST_SUCCESS',
        list
    }
}

export function fetchFriendsSerialsRatingsFailure(error) {
    return {
        type: 'FETCH_FRIENDS_RATINGS_REQUEST_FAILURE',
        error
    }
}

export function loadFriendsSerialsStatus(id) {
    return (dispatch) => {
        dispatch(fetchFriendsSerialsStatusRequested());
        return fetchFriendsSerialsStatuses(id)
            .then(response => {
                dispatch(fetchFriendsSerialsStatusSuccess(response))
            })
            .catch((error) => {
                dispatch(fetchFriendsSerialsStatusFailure(error))
            })
    }
}

export function loadFriendsSerialsRatings(id) {
    return (dispatch) => {
        dispatch(fetchFriendsSerialsRatingsRequested());
        return fetchFriendsSerialsRatings(id)
            .then(response => {
                dispatch(fetchFriendsSerialsRatingsSuccess(response))
            })
            .catch((error) => {
                dispatch(fetchFriendsSerialsRatingsFailure(error))
            })
    }
}
