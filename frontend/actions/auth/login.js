import {browserHistory} from 'react-router';
import {checkHttpStatus, parseJSON, RESPONSE_STATUS, USER_TYPE} from '../../lib/utils/utils';
import {loginUserRemote, loginUserMock} from './../../api/auth';
import {saveUserLocalStorage} from "../../lib/utils/localStorage";

export function loginUserRequested() {
    return {
        type: 'LOGIN_USER_REQUESTED'
    };
}

export function loginUserSuccess(user) {
    saveUserLocalStorage(user);
    return {
        type: 'LOGIN_USER_SUCCESS',
        user
    };
}

export function loginUserFailure(error) {
    return {
        type: 'LOGIN_USER_FAILURE',
        error
    };
}

export function loginUser(email, password, redirect = "/") {
    return function (dispatch) {
        dispatch(loginUserRequested());
        return loginUserRemote(email, password)
            .then(response => {
                if (response.message) {
                    dispatch(loginUserFailure(response))
                } else {
                    browserHistory.push(redirect);
                    Object.assign(response, {
                        type: 'Client'
                    });
                    dispatch(loginUserSuccess(response));
                }
            })
    }
}