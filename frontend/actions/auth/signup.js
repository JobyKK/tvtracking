import {browserHistory} from 'react-router';
import {checkHttpStatus, parseJSON, RESPONSE_STATUS} from '../../lib/utils/utils';
import {signUpUserMock, signUpUserRemote} from './../../api/auth';
import {loginUserSuccess} from "./login";

export function signUpClient(email, password) {
    browserHistory.push('/client');
    return {
        type: 'SIGNUP_CLIENT',
    };
}

export function signUpUserRequested() {
    return {
        type: 'SIGNUP_USER_REQUESTED'
    };
}

export function signUpUserSuccess() {
    return {
        type: 'SIGNUP_USER_SUCCESS'
    };
}

export function signUpUserFailure(error) {
    return {
        type: 'SIGNUP_USER_FAILURE',
        error
    };
}

export function signUpUser(name, email, password, redirect = "/") {
    return function (dispatch) {
        dispatch(signUpUserRequested());
        return signUpUserRemote(name, email, password)
            .then(response => {
                if (response.message) {
                    dispatch(signUpUserFailure(response))
                } else {
                    browserHistory.push(redirect);
                    dispatch(signUpUserSuccess());
                    dispatch(loginUserSuccess(...response));
                }
            })
    }
}
