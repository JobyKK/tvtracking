import { browserHistory } from 'react-router';
import { checkHttpStatus, parseJSON, RESPONSE_STATUS } from '../../lib/utils/utils';
import {logoutUserRemote, logoutUserMock} from './../../api/auth';
import {removeUserLocalStorage} from "../../lib/utils/localStorage";

export function logoutUserRequested() {
    return {
        type: 'LOGOUT_USER_REQUESTED'
    };
}

export function logoutUserSuccess() {
    removeUserLocalStorage();
    return {
        type: 'LOGOUT_USER_SUCCESS',
    };
}

export function logoutUserFailure() {
    return {
        type: 'LOGOUT_USER_FAILURE'
    };
}

export function logoutUser(redirect="/") {
    return function(dispatch) {
        dispatch(logoutUserRequested());
        return logoutUserRemote()
            .then(response => {
                browserHistory.push(redirect);
                dispatch(logoutUserSuccess());
            }).catch(error => {
                logoutUserFailure(error)
            })
    }
}