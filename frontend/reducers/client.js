import { Client } from 'model/client';

export function client(state = new Client({ eventCreatorName: 'example name' }), action) {
    switch (action.type) {
        case 'TEMP':
            return Object.assign({}, state, {
                isWaiterRequested: true,
            });
        default:
            return state;
    }
}
