import {FriendsSerialsInfo} from "../model/friendsSerialsInfo";

export function friendsSerialsInfo(state = new FriendsSerialsInfo(), action) {
    switch (action.type) {
        case 'FETCH_FRIENDS_SERIALS_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                statuses: action.list,
            });
        case 'FETCH_FRIENDS_RATINGS_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                ratings: action.list,
            });
        default:
            return state;
    }
}
