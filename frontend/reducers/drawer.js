import {Drawer} from '../model/drawer';

export function drawer(state = new Drawer(), action) {
    switch (action.type) {
        case 'TOGGLE_DRAWER':
            return Object.assign({}, state, {
                open: action.open
            });
        case 'CLOSE_DRAWER':
            return Object.assign({}, state, {
                open: false
            });
        default:
            return state;
    }
}