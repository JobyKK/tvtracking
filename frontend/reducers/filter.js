import {Filter} from './../model/filter';

export function showFilter(state = new Filter(), action) {
    switch (action.type) {
        case 'FILTER_STARTED':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'FILTER_FINISHED':
            return Object.assign({}, state, {
                isLoading: false
            });
        case 'UPDATE_RECOMMENDED_LIST_INPUT_SUCCESS':
            return Object.assign({}, state, {
                isLoading: false,
                recommendedShowList: action.list
            });
        case 'FILTER_NAME_CHANGED':
            return Object.assign({}, state, {
                isLoading: false,
                name: action.name,
                genreId: ""
            });
        case 'FILTER_GENRE_CHANGED':
            return Object.assign({}, state, {
                isLoading: false,
                genreId: action.genreId,
                name: ""
            });

        default:
            return state;
    }
}
