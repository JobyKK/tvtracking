import {FriendsList} from '../model/friendsList';

export function friendsList(state = new FriendsList(), action) {
    switch (action.type) {
        case 'FRIENDS_LIST_REQUESTED':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'FRIENDS_LIST_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                list: action.list,
                isLoading: false
            });
        case 'FRIENDS_LIST_REQUEST_FAILURE':
            return Object.assign({}, state, {
                isLoading: false
            });


        case 'OPEN_REMOVE_MODAL':
            return Object.assign({}, state, {
                removeModal: true,
                removeModalContent: action.name + ' (' + action.email + ')',
                removeFriendId: action.id
            });
        case 'CLOSE_REMOVE_MODAL':
            return Object.assign({}, state, {
                removeModal: false
            });


        case 'REMOVE_FRIEND_REQUESTED':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'REMOVE_FRIEND_REQUEST_SUCCESS':
            let listDel = state.list.filter(friend => {
                return friend.Person.UserID !== action.friend.person_id;
            });
            return Object.assign({}, state, {
                isLoading: false,
                list: listDel,
                removeModal: false
            });
        case 'REMOVE_FRIEND_REQUEST_FAILURE':
            return Object.assign({}, state, {
                isLoading: false,
                removeModal: false
            });
        case 'ADD_FRIEND_REQUEST_SUCCESS':
            let list = state.list;
            list.push(action.friend);
            return Object.assign({}, state, {
                isLoading: false,
                removeModal: false,
                list: list
            });


        default:
            return state;
    }
}