import {Show} from './../model/show';
import {SHOW_STATUS_TYPE} from "../lib/utils/utils";

export function show(state = new Show(), action) {
    switch (action.type) {
        // todo refactor other reducers
        case 'SHOW_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                ...action.show,
                isLoading: false
            });
        case 'SHOW_REQUESTED':
            return Object.assign({}, state, {
                id: null,
                userStatus: SHOW_STATUS_TYPE.UNSEEN.id,
                isLoading: true
            });
        case 'SHOW_REQUEST_FAILURE':
            return Object.assign({}, state, {
                isLoading: false
            });
        case 'UPDATE_SHOW_STATUS_SUCCESS':
            return Object.assign({}, state, {
                userStatus: action.status,
                isLoading: false
            });
        case 'UPDATE_SHOW_RATE_SUCCESS':
            return Object.assign({}, state, {
                userRating: action.rating,
                isLoading: false
            });
        case 'UPDATE_EPISODE_STATUS_SUCCESS':
            let seasons = state.seasons.map(season => {
                if (season.season_number == action.season) {
                    season.episodes = season.episodes.map(episode => {
                        if (episode.episode_number == action.episode) {
                            episode.userStatus = action.status;
                        }
                        return episode;
                    });
                }
                return season;
            });
            return Object.assign({}, state, {
                seasons,
                // status: actions
                isLoading: false
            });
        default:
            return state;
    }
}