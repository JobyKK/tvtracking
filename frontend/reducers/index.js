import {combineReducers} from 'redux';
import {client} from './client';
import {user, loginForm, signUpForm} from './auth';
import {showFilter} from './filter';
import {showList} from './showList';
import {show} from './show';
import {genres} from './genreList';
import {drawer} from './drawer';
import {friendsList} from './friendsList';
import {friendsSearch} from './friendsSearch';
import {friendsSerialsInfo} from "./friendsSerialsInfo";

const rootReducer = combineReducers({
    client,
    user,
    showFilter,
    showList,
    show,
    genres,
    loginForm,
    signUpForm,
    drawer,
    friendsList,
    friendsSearch,
    friendsSerialsInfo
});

export default rootReducer;
