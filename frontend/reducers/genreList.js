import {Genres} from './../model/genreList';

export function genres(state = new Genres(), action) {
    switch (action.type) {
        case 'GENRE_LIST_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                list: action.list.genres,
            });
        default:
            return state;
    }
}