import { User } from './../model/auth';
import { Client } from './../model/client';
import {USER_TYPE} from './../lib/utils/utils';
import {LoginForm, SignUpForm} from "../model/auth";

export function user(state = new User(), action) {
    switch (action.type) {
        case 'LOGOUT_USER_SUCCESS':
            return Object.assign({}, state, {
                type: USER_TYPE.ANONYMOUS,
                isAuthenticated: false,
                user: null
            });
        case 'LOGIN_USER_SUCCESS':
            return Object.assign({}, state, {
                isAuthenticated: true,
                user: new Client(action.user)
            });
        case 'SIGNUP_USER_SUCCESS':
            return Object.assign({}, state, {
                isAuthenticated: true,
            });
        case 'LOGIN_CLIENT':
            return Object.assign({}, state, {
                user: Client,
            });
        default:
            return state;
    }
}

export function loginForm(state = new LoginForm(), action) {
    switch (action.type) {
        case 'LOGIN_USER_FAILURE':
            return Object.assign({}, state, {
                serverError: action.error,
            });
        case 'LOGIN_USER_SUCCESS':
            return Object.assign({}, state, {
                serverError: {},
            });
        default:
            return state;
    }
}

export function signUpForm(state = new SignUpForm(), action) {
    switch (action.type) {
        case 'SIGNUP_USER_FAILURE':
            return Object.assign({}, state, {
                serverError: action.error,
            });
        case 'SIGNUP_USER_SUCCESS':
            return Object.assign({}, state, {
                serverError: {},
            });
        default:
            return state;
    }
}