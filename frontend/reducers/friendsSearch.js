import {FriendsSearch} from '../model/friendsSearch';

export function friendsSearch(state = new FriendsSearch(), action) {
    switch (action.type) {
        case 'CHANGE_SEARCH':
            return Object.assign({}, state, {
                searchField: action.value
            });


        case 'SEARCH_FRIEND_REQUESTED':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'SEARCH_FRIEND_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                isLoading: false,
                addModal: true,
                addModalContent: action.friend.name + ' (' + action.friend.email + ')',
                addModalTitle: 'Add this user to your friends list?',
                addFriendId: action.friend.id,
                findSuccess: true
            });
        case 'SEARCH_FRIEND_REQUEST_FAILURE':
            return Object.assign({}, state, {
                isLoading: false,
                addModal: true,
                addModalContent: 'No user found with this email.',
                addModalTitle: 'Oops...',
                addFriendId: undefined,
                findSuccess: false
            });


        case 'CLOSE_ADD_MODAL':
            return Object.assign({}, state, {
                addModal: false
            });


        case 'ADD_FRIEND_REQUESTED':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'ADD_FRIEND_REQUEST_SUCCESS':
            return Object.assign({}, state, {
                isLoading: false,
                addModal: false,
                searchField: ''
            });
        case 'ADD_FRIEND_REQUEST_FAILURE':
            return Object.assign({}, state, {
                isLoading: false,
                addModal: false,
                searchField: ''
            });


        default:
            return state;
    }
}