//-------------Create express app-------------
const express = require('express');
const session = require('express-session');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const webpack = require('webpack');
const config = require('./webpack.prod.config.js');
const compiler = webpack(config);

const defaultUser = 'default';

// Set up the express app
const app = express();

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
}));

app.use(require('webpack-hot-middleware')(compiler));

// Log requests to the console
app.use(logger('dev'));

// Must use cookieParser before session
app.use(cookieParser());

// Initialize session
app.use(session({
    secret: 'ssshhhhh',
    saveUninitialized: true,
    resave: true
}));

// Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Engine

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// Routes
const apiUser = require('./backend/routes/user');
app.use('/api/user', apiUser);

const apiGenre = require('./backend/routes/genre');
app.use('/api/genre', apiGenre);

const apiTV = require('./backend/routes/tv');
app.use('/api/tv', apiTV);

const apiFollow = require('./backend/routes/follow');
app.use('/api/follow', apiFollow);

const apiTVStatus = require('./backend/routes/tv_status');
app.use('/api/tvstatus', apiTVStatus);

const apiEpisodeStatus = require('./backend/routes/episode_status');
app.use('/api/episode', apiEpisodeStatus);

const apiTVRating = require('./backend/routes/tv_rating');
app.use('/api/tvrating', apiTVRating);

const apiProfile = require('./backend/routes/profile');
app.use('/api/profile', apiProfile);
 
const apiFriend = require('./backend/routes/friend');
app.use('/api/friend', apiFriend);

app.get('*', function (req, res) {
    if (!req.session.user) req.session.user = defaultUser;
    res.sendFile(path.join(__dirname, 'index.html'));
});

//-------------Create server using express app-------------

const http = require('http');

const port = parseInt(process.env.PORT, 10) || 80;
app.set('port', port);

const server = http.createServer(app);
server.listen(port, function (err) {
    if (!err) {
        console.log('Server listening at port %d', port);
    }
    else {
        console.error('Error starting web-server on port', port, err.toString());
        process.exit(99);
    }
});
