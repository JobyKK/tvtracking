const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js');
const _mdb = require('moviedb')('b227c496d142da161ca31bdf56e487d8');

const defaultUser = 'default';

const mdb = (m, q) => new Promise((res, rej) => {
  _mdb[m](q, (err, data) => err ? rej(err) : res(data))
});

//---------------------API TVSHows---------------------

router.get('/popular/:page', (req, res) => {
  mdb('miscPopularTvs', { page: req.params.page })
    .then((tvshows) => TVListInfo(res, tvshows))
    .catch((error) => res.status(400).send(error));
});

router.get('/:tvId', (req, res) => {
  mdb('tvInfo', { id: req.params.tvId })
    .then((tvShow) => TVInfo(res, tvShow, (tv) => {
        if (req.session.user === undefined || req.session.user === null || req.session.user === defaultUser)
          return res.status(200).send(tv);
        else return addStatusInfo(res, tv, req.session.user, req.params.tvId);
      })
    )
    .catch((error) => res.status(400).send(error));
});

router.get('/:tvId/season/:seasonNumber', (req, res) => {
  mdb('tvSeasonInfo', { id: req.params.tvId, season_number: req.params.seasonNumber })
    .then((season) => res.status(200).send(season))
    .catch((error) => res.status(400).send(error));
});

router.get('/:tvId/season/:seasonNumber/episode/:episodeNumber', (req, res) => {
  mdb('tvEpisodeInfo', { id: req.params.tvId, season_number: req.params.seasonNumber,
    episode_number: req.params.episodeNumber })
    .then((episode) => res.status(200).send(episode))
    .catch((error) => res.status(400).send(error));
});

//---------------------API Search TVSHows---------------------

// params: query - a text query to search
// params: page - result page number (20 items on one page)
router.get('/searchByName/:query/:page', (req, res) => {
  mdb('searchTv', { query: req.params.query, page: req.params.page })
    .then((tvshows) => TVListInfo(res, tvshows))
    .catch((error) => res.status(400).send(error));
});

// params: page - result page number (20 items on one page)
router.get('/searchByYear/:year/:page', (req, res) => {
  mdb('discoverTv', { first_air_date_year: req.params.year, page: req.params.page })
    .then((tvshows) => TVListInfo(res, tvshows))
    .catch((error) => res.status(400).send(error));
});

// params: genres - comma separated value of genre ids
// params: page - result page number (20 items on one page)
router.get('/searchByGenres/:genres/:page', (req, res) => {
  mdb('discoverTv', { with_genres: req.params.genres, page: req.params.page })
    .then((tvshows) => TVListInfo(res, tvshows))
    .catch((error) => res.status(400).send(error));
});

// params: count - number of top tvshows names in list (should be divided by 20)
router.get('/top/:count', (req, res) => topTVNames(res, req.params.count));

/* --------------------------------------------------------------------------------------------------------*/
/* --------------------------------------------- Stat API -------------------------------------------------*/
/* --------------------------------------------------------------------------------------------------------*/

function TVInfo(res, tvshow, callback) {
  let seasons = [];
  let promises = tvshow.seasons.map((season) => {
    return mdb('tvSeasonInfo', { id: tvshow.id, season_number: season.season_number });
  });

  Promise.all(promises)
    .then((data) => {
      data.forEach((season) => {
        let episodes = season.episodes.map((episode) => {
          return {
            episode_number: episode.episode_number,
            name: episode.name,
            date: episode.air_date 
          }
        });
        seasons.push({
          season_number: season.season_number,
          name: season.name,
          overview: season.overview,
          date: season.air_date,
          episodes: episodes
        });
      });

      return callback({
        id: tvshow.id,
        name: tvshow.name,
        poster: tvshow.poster_path ? 'https://image.tmdb.org/t/p/w500' + tvshow.poster_path : null,
        overview: tvshow.overview,
        genres: tvshow.genres,
        country: tvshow.origin_country,
        date: tvshow.first_air_date,
        number_of_seasons: tvshow.number_of_seasons,
        number_of_episodes: tvshow.number_of_episodes,
        status: tvshow.status,
        seasons: seasons
      });
    })
    .catch((error) => res.status(400).send(error));
}

function TVListInfo(res, tvlist) {
  let tvshows = tvlist.results.map((tvshow) => {
    return {
      id: tvshow.id,
      name: tvshow.name,
      country: tvshow.origin_country,
      date: tvshow.first_air_date,
      poster: tvshow.poster_path ? 'https://image.tmdb.org/t/p/w500' + tvshow.poster_path : null
    };
  });
  tvlist.results = tvshows;
  res.status(200).send(tvlist);
}

function topTVNames(res, count) {
  let promises = [];
  for(let i = 1; i <= (count / 20); ++i)
    promises.push(mdb('miscPopularTvs', { page: i })); 
      
  Promise.all(promises)
    .then((pages) => {
      let tvlist = [];
      pages.forEach((page) => {
        page.results.forEach((tvshow) => {
          tvlist.push({
            id: tvshow.id,
            name: tvshow.name
          });
        });
      });
      res.status(200).send(tvlist);
    })
    .catch((error) => res.status(400).send(error));
}

function addStatusInfo (res, tv, user, tvId) {
  Promise.all([controllers.episode_statuses.getByTV(user, tvId), controllers.tv_statuses.get(user, tvId), controllers.tv_ratings.get(user, tvId)])
  .then((status) => {
    let episodeStat = [];
    if (status[0].length > 0)
      for (let i = 0; i < status[0].length; ++i) {
        episodeStat.push(status[0][i].dataValues);
      }
    let tvShowStat = status[1] ? status[1].dataValues.Status || 1 : 1;
    let tvShowRat = status[2] ? status[2].dataValues.Rating || 0 : 0;

    tv.userStatus = tvShowStat;
    tv.userRating = tvShowRat;

    for (let i = 0; i < tv.seasons.length; ++i) {
      let season = tv.seasons[i];

      for (let j = 0; j < season.episodes.length; ++j) {
        let episode = season.episodes[j];
        episode.userStatus = false;

        for (let k = 0; k < episodeStat.length; ++k) {

          if (episodeStat[k].SeasonNumber === season.season_number && 
              episodeStat[k].EpisodeNumber === episode.episode_number) {
            episode.userStatus = episodeStat[k].IsViewed;
            break;
          }
        }
      }
    }
    return res.status(200).send(tv);
  })
  .catch((error) => res.status(400).send(error));
}

module.exports = router;
