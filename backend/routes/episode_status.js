const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js').episode_statuses;

const defaultUser = 'default';

//---------------------API Episodes Statuses---------------------

router.get('/list', controllers.list);

// req body params: tv_id, season, episode
router.post('/add', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.create(res, req.session.user, req.body.tv_id, req.body.season, req.body.episode);
});

// req body params: tv_id, season, episode
router.delete('/delete', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.delete(res, req.session.user, req.body.tv_id, req.body.season, req.body.episode);
});

// params: tv_id, season, episode
router.get('/status/:tv_id/:season/:episode', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.get(res, req.session.user, req.params.tv_id, req.params.season, req.params.episode);
});

router.get('/status/byTV/:tv_id/', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.getByTV(res, req.session.user, req.params.tv_id);
});

module.exports = router;