const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js');

const defaultUser = 'default';

//---------------------API Friends---------------------

// params: friend_id
router.get('/tvlist/:friend_id', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  Promise.all([controllers.tv_statuses.getTVByStatus(req.params.friend_id, 2),
    controllers.tv_statuses.getTVByStatus(req.params.friend_id, 3),
    controllers.tv_statuses.getTVByStatus(req.params.friend_id, 4),
    controllers.tv_statuses.getTVByStatus(req.params.friend_id, 5)])
  .then((tv) => {
      tvlist = {};
      tv.forEach(function(t, i) {
        if(t.length > 0) {
            var status = String(i+2);
            tvlist[status] = [];
            t.forEach(function(e){
                tvlist[status].push(e.dataValues);
            })
        }
      });

      return res.status(200).send(tvlist);
  })
  .catch((error) => res.status(400).send(error));
});

// params: friend_id
router.get('/tvrating/:friend_id', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.tv_ratings.getRatingByUser(res, req.params.friend_id);
});

module.exports = router;