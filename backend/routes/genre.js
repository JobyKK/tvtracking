const express = require('express');
const router = express.Router();
const _mdb = require('moviedb')('b227c496d142da161ca31bdf56e487d8');

const mdb = (m, q) => new Promise((res, rej) => {
  _mdb[m](q, (err, data) => err ? rej(err) : res(data))
});

//---------------------API Genres---------------------

router.get('/list', (req, res) => {
  mdb('genreTvList')
    .then((genres) => res.status(201).send(genres))
    .catch((error) => res.status(400).send(error));
});

module.exports = router;