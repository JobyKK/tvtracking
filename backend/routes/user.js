const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js').users;

const defaultUser = 'default';

//---------------------API Users---------------------

router.get('/list', controllers.list);

// search users by params: email
router.get('/search/:email', controllers.search);

// req body params: name, email, password
router.post('/create', (req, res) => {
  if (!req.session.user) req.session.user = defaultUser;
  if (req.session.user != defaultUser)
    return res.status(409).send({
      message: `User ${req.session.user} has already loggen in`,
    });
  
  if (validateCreate(req, res)) {
    controllers.create(req, res);
  }
});

// req body params: email, password
router.post('/login', (req, res) => {
  if (!req.session.user) req.session.user = defaultUser;
  if (req.session.user != defaultUser) 
    return res.status(409).send({
      message: `User ${req.session.user} has already loggen in`,
    });
  
  controllers.login(req, res);
});

router.post('/logout', (req, res) => {
  req.session.user = defaultUser;
  res.status(200).send({user: req.session.user});
})

// req body params: name | email | password
// router.put('/update', controllers.users.update);

/* --------------------------------------------------------------------------------------------------------*/
/* --------------------------------------------- Stat API -------------------------------------------------*/
/* --------------------------------------------------------------------------------------------------------*/

function validateCreate(req, res) {
  //let password = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{5,20}$/;
  let password = /^\S{5,20}$/;
  //(?=.*[0-9]) - Assert a string has at least one number
  //(?=.*[!@#$%^&*]) - Assert a string has at least one special character
  let email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})$/;
  let name = /^[\w-]{5,25}$/;
  if (!password.test(req.body.password)) {
    res.status(406).send({message: 'Bad password'});
    return false;
  }
  if (!email.test(req.body.email)) {
    res.status(406).send({message: 'Bad email'});
    return false;
  }
  if (!name.test(req.body.name))  {
    res.status(406).send({message: 'Bad name'});
    return false;
  }
  return true;
}

module.exports = router;