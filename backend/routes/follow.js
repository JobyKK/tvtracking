const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js').follows;

const defaultUser = 'default';

//---------------------API Follows---------------------

router.get('/list', controllers.list);

// req body params: person_id
router.post('/add', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.create(res, req.session.user, req.body.person_id);
});

// req body params: person_id
router.delete('/delete', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.delete(res, req.session.user, req.body.person_id);
});

// get all friends of the current user
router.get('/friends', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.friends(res, req.session.user);
});

module.exports = router;