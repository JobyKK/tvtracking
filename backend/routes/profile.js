const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js');
const _mdb = require('moviedb')('b227c496d142da161ca31bdf56e487d8');

const defaultUser = 'default';

const mdb = (m, q) => new Promise((res, rej) => {
  _mdb[m](q, (err, data) => err ? rej(err) : res(data))
});

router.get('/episodes', (req, res) => {
    const user = req.session.user;
    if (!user || user === defaultUser) {
        res.status(401).send('no authorized');
    }
    else {
        controllers.tv_statuses.getTVByStatus(user, 2)
            .then((result) => {
                if (!result || result.error) {
                    res.status(400).send(!result ? 'unknown error' : result.error ? result.error : undefined);
                }
                else {
                    let tvIds = [];
                    let output = [];
                    let promises = [];

                    for (let i = 0; i < result.length; ++i) {
                        tvIds[i] = result[i].dataValues.TVShowID;
                        promises.push(mdb('tvInfo', {id: tvIds[i]}));
                        promises.push(controllers.episode_statuses.getByTV(user, tvIds[i]));
                    }  

                    Promise.all(promises)
                        .then((results) => {
                            let episodesInfo = [];
                            let tvInfo = [];
                            for (let i = 0; i < results.length; ++i) {
                                if (Array.isArray(results[i])) {
                                    if (results[i]) {
                                        for (j = 0; j < results[i].length; ++j) {
                                            episodesInfo.push({
                                                id: results[i][j].dataValues.TVShowID,
                                                season: results[i][j].dataValues.SeasonNumber,
                                                episode: results[i][j].dataValues.EpisodeNumber
                                            });
                                        }
                                    }
                                }
                                else {
                                    tvInfo.push({
                                        id: results[i].id,
                                        name: results[i].name,
                                        seasons: results[i].seasons.map((season) => {
                                            return {
                                                season_number: season.season_number
                                            };
                                        })
                                    });
                                }
                            }
                            
                            let promisesEpisodes = [];
                            for (let i = 0; i < tvInfo.length; ++i) {
                                for (let j = 0; j < tvInfo[i].seasons.length; ++j) {
                                    promisesEpisodes.push(mdb('tvSeasonInfo', { id: tvInfo[i].id, season_number: tvInfo[i].seasons[j].season_number}));
                                }
                            }
                            Promise.all(promisesEpisodes)
                                .then((results) => {
                                    for (let k = 0, i = 0; i < tvInfo.length; ++i) {
                                        for (let j = 0; j < tvInfo[i].seasons.length; ++j, ++k) {
                                            tvInfo[i].seasons[j].season_name = results[k].name;
                                            tvInfo[i].seasons[j].episodes = results[k].episodes.map((episode) => {
                                                return {
                                                    episode: episode.episode_number,
                                                    name: episode.name,
                                                    date: episode.air_date 
                                                };
                                            });
                                        }
                                    }
                                    
                                    for (let i = 0; i < tvInfo.length; ++i) {
                                        let seasons = [];

                                        for (let j = 0; j < tvInfo[i].seasons.length; ++j) {
                                            let episodes = tvInfo[i].seasons[j].episodes.filter((ep) => {
                                                for (let k = 0; k < episodesInfo.length; ++k) {
                                                    if ((tvInfo[i].id === episodesInfo[k].id &&
                                                        tvInfo[i].seasons[j].season_number === episodesInfo[k].season &&
                                                        ep.episode === episodesInfo[k].episode) || 
                                                        (new Date(ep.date) > new Date())) {
                                                            return false;
                                                        }
                                                }
                                                return true;
                                            });

                                            if (episodes.length > 0) {
                                                seasons.push({
                                                    season_number: tvInfo[i].seasons[j].season_number,
                                                    season_name: tvInfo[i].seasons[j].season_name,
                                                    episodes: episodes
                                                });
                                            }
                                        }
                                        if (seasons.length > 0) {
                                            output.push({
                                                id: tvInfo[i].id,
                                                name: tvInfo[i].name,
                                                seasons: seasons
                                            });
                                        }
                                    }
                                    res.status(200).send(output);
                                })
                                .catch((error) => res.status(400).send(error))
                        })
                        .catch((error) => {
                            res.status(400).send(error);
                        });             
                }
        })
        .catch((error) => res.status(400).send(error))
    }
});

router.get('/calendar', (req, res) => {
    const user = req.session.user;
    if (!user || user === defaultUser) {
        res.status(401).send('no authorized');
    }
    else {
        controllers.tv_statuses.getTVByStatus(user, 2) 
        .then((result) => {
            if (!result || result.error) {
                res.status(400).send(!result ? 'unknown error' : result.error ? result.error : undefined);
            }
            else {
                let tvIds = [];
                let output = [];
                let promises = [];

                for (let i = 0; i < result.length; ++i) {
                    tvIds[i] = result[i].dataValues.TVShowID;
                    promises.push(mdb('tvInfo', {id: tvIds[i]}));
                }  

                Promise.all(promises)
                    .then((results) => {
                        let tvInfo = [];
                        for (let i = 0; i < results.length; ++i) {
                            tvInfo.push({
                                id: results[i].id,
                                name: results[i].name,
                                seasons: results[i].seasons.map((season) => {
                                    return {
                                        season_number: season.season_number
                                    };
                                })
                            });
                        }
                        let promisesEpisodes = [];
                        for (let i = 0; i < tvInfo.length; ++i) {
                            for (let j = 0; j < tvInfo[i].seasons.length; ++j) {
                                promisesEpisodes.push(mdb('tvSeasonInfo', { id: tvInfo[i].id, season_number: tvInfo[i].seasons[j].season_number}));
                            }
                        }
                        Promise.all(promisesEpisodes)
                            .then((results) => {
                                for (let k = 0, i = 0; i < tvInfo.length; ++i) {
                                    for (let j = 0; j < tvInfo[i].seasons.length; ++j, ++k) {
                                        tvInfo[i].seasons[j].season_name = results[k].name;
                                        tvInfo[i].seasons[j].episodes = results[k].episodes.map((episode) => {
                                            return {
                                                episode: episode.episode_number,
                                                name: episode.name,
                                                date: episode.air_date 
                                            };
                                        });
                                    }
                                }
                                
                                for (let i = 0; i < tvInfo.length; ++i) {
                                    let seasons = [];

                                    for (let j = 0; j < tvInfo[i].seasons.length; ++j) {
                                        let episodes = tvInfo[i].seasons[j].episodes.filter((ep) => {
                                            return new Date(ep.date) > new Date();
                                        });

                                        if (episodes.length > 0) {
                                            seasons.push({
                                                season_number: tvInfo[i].seasons[j].season_number,
                                                season_name: tvInfo[i].seasons[j].season_name,
                                                episodes: episodes
                                            });
                                        }
                                    }
                                    if (seasons.length > 0) {
                                        output.push({
                                            id: tvInfo[i].id,
                                            name: tvInfo[i].name,
                                            seasons: seasons
                                        });
                                    }
                                }
                                res.status(200).send(output);
                            })
                            .catch((error) => res.status(400).send(error))
                    })
                    .catch((error) => {
                        res.status(400).send(error);
                    });             
            }
        })
        .catch((error) => res.status(400).send(error))
    }
});

module.exports = router;