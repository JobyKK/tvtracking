const express = require('express');
const router = express.Router();
const db = require('./../db_server/models/index.js');
const controllers = require('./../db_server/controllers/index.js').tv_ratings;

const defaultUser = 'default';

//---------------------API TVSHows Statuses---------------------

router.get('/list', controllers.list);

// req body params: tv_id, rating (1, 2, 3, 4, 5)
router.post('/set', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.set(res, req.session.user, req.body.tv_id, req.body.rating, req.body.tv_name);
});

// params: tv_id
router.get('/get/:tv_id', (req, res) => {
  if ((req.session.user == defaultUser) || !req.session.user)
    return res.status(401).send({
      message: 'Current User Not Found'
    });
  controllers.get(res, req.session.user, req.params.tv_id);
});

module.exports = router;