module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('EpisodeStatuses', {
      EpisodeStatusID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      UserID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'UserID',
          as: 'UserID',
        }
      },
      EpisodeNumber: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      SeasonNumber: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      TVShowID: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      IsViewed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      CRTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      UPTime: {
        type: Sequelize.DATE,
        allowNull: true
      }
    }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('EpisodeStatuses'),
};