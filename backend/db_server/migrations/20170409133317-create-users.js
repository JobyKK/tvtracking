module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('Users', {
      UserID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      Name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      Email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      Password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      CRTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      UPTime: {
        type: Sequelize.DATE,
        allowNull: true
      }
    }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Users'),
};