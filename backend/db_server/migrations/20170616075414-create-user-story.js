module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('UserStories', {
      UserStoryID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      UserID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'UserID',
          as: 'UserID',
        }
      },
      Status: {
        type: Sequelize.STRING,
        allowNull: false
      },
      CRTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      UPTime: {
        type: Sequelize.DATE,
        allowNull: true
      }
    }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('UserStories'),
};