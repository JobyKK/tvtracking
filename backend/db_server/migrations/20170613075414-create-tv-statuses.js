module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('TVStatuses', {
      TVStatusID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      UserID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'UserID',
          as: 'UserID',
        }
      },
      TVShowID: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      TVShowName: {
        type: Sequelize.STRING,
        allowNull: true
      },
      Status: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      CRTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      UPTime: {
        type: Sequelize.DATE,
        allowNull: true
      }
    }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('TVStatuses'),
};