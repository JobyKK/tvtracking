module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('TVRatings', {
      TVRatingID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      UserID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'UserID',
          as: 'UserID',
        }
      },
      TVShowID: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      TVShowName: {
        type: Sequelize.STRING,
        allowNull: true
      },
      Rating: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      CRTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      UPTime: {
        type: Sequelize.DATE,
        allowNull: true
      }
    }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('TVRatings'),
};