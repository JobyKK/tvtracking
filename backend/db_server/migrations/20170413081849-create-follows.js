module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('Follows', {
      FollowID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      FollowerID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'UserID',
          as: 'FollowerID',
        }
      },
      PersonID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'UserID',
          as: 'PersonID',
        }
      },
      IsFollow: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      CRTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      UPTime: {
        type: Sequelize.DATE,
        allowNull: true
      }
    }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Follows'),
};