const users = require('./users');
const follows = require('./follows');
const tv_statuses = require('./tv_statuses');
const episode_statuses = require('./episode_statuses');
const tv_ratings = require('./tv_ratings');
const user_stories = require('./user_stories');

module.exports = {
  users,
  follows,
  tv_statuses,
  episode_statuses,
  tv_ratings,
  user_stories
};