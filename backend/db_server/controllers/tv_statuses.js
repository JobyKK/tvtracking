const TVStatus = require('../models').TVStatus;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return TVStatus
      .all()
      .then((TVStatuses) => res.status(200).send(TVStatuses))
      .catch((error) => res.status(400).send(error));
  },
  set(res, user_id, tv_id, status, tv_name) {
    return TVStatus
      .findOne({
        where: {
          TVShowID: tv_id,
          UserID: user_id
        }
      })
      .then((tvStatus) => {
        if (!tvStatus) {
          return TVStatus
            .create({
              TVShowID: tv_id,
              TVShowName: tv_name,
              UserID: user_id,
              Status: status,
              CRTime: new Date()
            })
            .then((tvStatus) => res.status(201).send(tvStatus))
            .catch((error) => res.status(400).send(error));
        }
        return tvStatus
          .update({
            Status: status,
            UPTime: new Date()
          })
          .then((tvStatus) => res.status(200).send(tvStatus))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
  get(user_id, tv_id) {
    return TVStatus
      .findOne({
        attributes: ['Status'],
        where: {
          TVShowID: tv_id,
          UserID: user_id
        }
      })
      //.then((tvStatus) => res.status(200).send(tvStatus))
      //.catch((error) => res.status(400).send(error));
  },
  getTVByStatus(user_id, status) {
    return TVStatus
      .findAll({
        attributes: ['TVShowID', 'TVShowName'],
        where: {
          UserID: user_id,
          Status: status
        }
      })
      //.then((tvStatus) => callback(tvStatus))
      //.catch((error) => callback({error: error}));
  }
};