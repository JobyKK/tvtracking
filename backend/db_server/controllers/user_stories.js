const UserStory = require('../models').UserStory;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return UserStory
      .all()
      .then((UserStories) => res.status(200).send(UserStories))
      .catch((error) => res.status(400).send(error));
  },
  set(res, user_id, status) {
    return UserStory
        .create({
            UserID: user_id,
            Status: status,
            CRTime: new Date()
        })
        .then((userStory) => res.status(201).send(data))
        .catch((error) => res.status(400).send(error));
  },
  get(user_id) {
    return UserStory
      .findAll({
        attributes: ['Story', 'CRTime'],
        where: {
          UserID: user_id
        },
        order: [
            ['CRTime', 'DESC'],
        ]
      })
      //.then((tvStatus) => callback(tvStatus))
      //.catch((error) => callback({error: error}));
  }
};