const User = require('../models').User;

module.exports = {
  list(req, res) {
    return User
      .all()
      .then((users) => res.status(200).send(users))
      .catch((error) => res.status(400).send(error));
  },
  search(req, res) {
    return User
      .findOne({
        where: {
          Email: req.params.email
        },
        attributes: [['UserID', 'id'], ['Name', 'name'], ['Email', 'email']] 
      })
      .then((user) => res.status(200).send(user))
      .catch((error) => res.status(400).send(error));
  },
  create(req, res) {
    return User
      .findOrCreate({
        where: {
          Email: req.body.email
        },
        defaults: {
          Name: req.body.name,
          Password: req.body.password,
          CRTime: new Date()
        }
      })
      .spread((user, created) => {
        if (!created) {
          return res.status(409).send({
            message: 'Email already exists'
          });
        }
        req.session.user = user.UserID;
        return res.status(201).send({
          id: user.UserID,
          name: user.Name,
          email: user.Email
        });
      })
      .catch((error) => res.status(400).send(error));
  },
  update(req, res) {
    return User
      .findById(req.body.id)
      .then((user) => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        return user
          .update({
            Name: req.body.name || user.Name,
            Email: req.body.email || user.Email,
            Password: req.body.password || user.Password,
            UPTime: new Date()
          })
          .then(() => res.status(200).send(user)) 
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
  login(req, res) {
    return User
      .findOne({
        where: {
          Email: req.body.email,
          Password: req.body.password 
        }
      })
      .then((user) => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found'
          });
        }
        req.session.user = user.UserID;
        return res.status(200).send({
          id: user.UserID,
          name: user.Name,
          email: user.Email
        });
      })
      .catch((error) => res.status(400).send(error));
  }
};