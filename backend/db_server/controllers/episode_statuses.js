const EpisodeStatus = require('../models').EpisodeStatus;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return EpisodeStatus
      .all()
      .then((episodeStatus) => res.status(200).send(episodeStatus))
      .catch((error) => res.status(400).send(error));
  },
  create(res, user_id, tv_id, season, episode) {
    return EpisodeStatus
      .findOne({
        where: {
          TVShowID: tv_id,
          SeasonNumber: season,
          EpisodeNumber: episode,
          UserID: user_id
        }
      })
      .then((episodeStatus) => {
        if (!episodeStatus) {
          return EpisodeStatus
            .create({
              TVShowID: tv_id,
              SeasonNumber: season,
              EpisodeNumber: episode,
              UserID: user_id,
              CRTime: new Date()
            })
            .then((episodeStatus) => res.status(201).send(episodeStatus))
            .catch((error) => res.status(400).send(error));
        }
        return episodeStatus
          .update({
            IsViewed: true,
            UPTime: new Date()
          })
          .then((episodeStatus) => res.status(200).send(episodeStatus))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
  delete(res, user_id, tv_id, season, episode) {
    return EpisodeStatus
      .update({ 
        IsViewed: false,
        UPTime: new Date()
      }, {
        where: {
          TVShowID: tv_id,
          SeasonNumber: season,
          EpisodeNumber: episode,
          UserID: user_id
        }
      })
      .then((episodeStatus) => res.status(200).send(episodeStatus))
      .catch((error) => res.status(400).send(error));
  },
  get(res, user_id, tv_id, season, episode) {
    return EpisodeStatus
      .findOne({
        attributes: ['IsViewed'],
        where: {
          TVShowID: tv_id,
          SeasonNumber: season,
          EpisodeNumber: episode,
          UserID: user_id
        }
      })
      .then((episodeStatus) => res.status(200).send(episodeStatus))
      .catch((error) => res.status(400).send(error));
  },
  getByTV(user_id, tv_id) {
    return EpisodeStatus
      .findAll({
        attributes: ['IsViewed', 'TVShowID', 'SeasonNumber', 'EpisodeNumber'],
        where: {
          TVShowID: tv_id,
          UserID: user_id,
          IsViewed: true
        }
      });
      //.then((episodeStatuses) => res.status(200).send(episodeStatuses))
      //.catch((error) => res.status(400).send(error));
  }
};