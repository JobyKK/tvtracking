const Follow = require('../models').Follow;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return Follow
      .all()
      .then((follows) => res.status(200).send(follows))
      .catch((error) => res.status(400).send(error));
  },
  create(res, follower_id, person_id) {
    return Follow
      .findOne({
        where: {
          FollowerID: follower_id,
          PersonID: person_id
        },
          include: [{
              model: User,
              as: 'Person',
              attributes: ['UserID', 'Name', 'Email']
          }]
      })
      .then((follow) => {
        if (!follow) {
          return Follow
            .create({
              FollowerID: follower_id,
              PersonID: person_id,
              CRTime: new Date()
            })
            .then((follow) => res.status(201).send(follow))
            .catch((error) => res.status(400).send(error));
        }
        return follow
          .update({
            IsFollow: true,
            UPTime: new Date()
          })
          .then((follow) => res.status(200).send(follow))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
  delete(res, follower_id, person_id) {
    return Follow
      .update({
        IsFollow: false,
        UPTime: new Date()
      }, {
        where: {
          FollowerID: follower_id,
          PersonID: person_id
        }
      })
      .then((follow) => res.status(200).send({person_id}))
      .catch((error) => res.status(400).send(error));
  },
  friends(res, follower_id) {
    return Follow
      .findAll({
        attributes: [],
        where: {
          FollowerID: follower_id,
          IsFollow: true
        },
        include: [{
          model: User,
          as: 'Person',
          attributes: ['UserID', 'Name', 'Email']
        }]
      })
      .then((friends) => res.status(200).send(friends))
      .catch((error) => res.status(400).send(error));
  }
};