const TVRating = require('../models').TVRating;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return TVRating
      .all()
      .then((TVRatings) => res.status(200).send(TVRatings))
      .catch((error) => res.status(400).send(error));
  },
  set(res, user_id, tv_id, rating, tv_name) {
    return TVRating
      .findOne({
        where: {
          TVShowID: tv_id,
          UserID: user_id
        }
      })
      .then((tvRating) => {
        if (!tvRating) {
          return TVRating
            .create({
              TVShowID: tv_id,
              TVShowName: tv_name,
              UserID: user_id,
              Rating: rating,
              CRTime: new Date()
            })
            .then((tvRating) => res.status(201).send(tvRating))
            .catch((error) => res.status(400).send(error));
        }
        return tvRating
          .update({
            Rating: rating,
            UPTime: new Date()
          })
          .then((tvRating) => res.status(200).send(tvRating))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
  get(user_id, tv_id) {
    return TVRating
      .findOne({
        attributes: ['Rating'],
        where: {
          TVShowID: tv_id,
          UserID: user_id
        }
      })
      //.then((tvStatus) => res.status(200).send(tvStatus))
      //.catch((error) => res.status(400).send(error));
  },
  getRatingByUser(res, user_id) {
    return TVRating
      .findAll({
        attributes: ['TVShowID', 'TVShowName', 'Rating'],
        where: {
          UserID: user_id
        }
      })
      .then((tvRatings) => res.status(200).send(tvRatings))
      .catch((error) => res.status(400).send(error));
  }
};