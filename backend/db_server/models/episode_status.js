module.exports = (sequelize, DataTypes) => {
  const EpisodeStatus = sequelize.define('EpisodeStatus', {
    EpisodeStatusID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    EpisodeNumber: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    SeasonNumber: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    TVShowID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    IsViewed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    CRTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    UPTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    classMethods: {
      associate: (models) => {
        EpisodeStatus.belongsTo(models.User, {
          foreignKey: 'UserID',
          as: 'User'
        });
      }
    }
  });
  return EpisodeStatus;
};