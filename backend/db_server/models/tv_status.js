module.exports = (sequelize, DataTypes) => {
  const TVStatus = sequelize.define('TVStatus', {
    TVStatusID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    TVShowID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    TVShowName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CRTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    UPTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    classMethods: {
      associate: (models) => {
        TVStatus.belongsTo(models.User, {
          foreignKey: 'UserID',
          as: 'User'
        });
      }
    }
  });
  return TVStatus;
};