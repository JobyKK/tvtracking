module.exports = (sequelize, DataTypes) => {
  const UserStory = sequelize.define('UserStory', {
    UserStoryID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    Story: {
      type: DataTypes.STRING,
      allowNull: true
    },
    CRTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    UPTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    classMethods: {
      associate: (models) => {
        UserStory.belongsTo(models.User, {
          foreignKey: 'UserID',
          as: 'User'
        });
      }
    }
  });
  return UserStory;
};