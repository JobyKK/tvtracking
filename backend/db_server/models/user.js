module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    UserID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    CRTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    UPTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    classMethods: {
      associate: (models) => {
        User.hasMany(models.Follow, {
          foreignKey: 'FollowerID'
        });
        User.hasMany(models.Follow, {
          foreignKey: 'PersonID'
        });
        User.hasMany(models.TVStatus, {
          foreignKey: 'UserID'
        });
        User.hasMany(models.EpisodeStatus, {
          foreignKey: 'UserID'
        });
        User.hasMany(models.TVRating, {
          foreignKey: 'UserID'
        });
        User.hasMany(models.UserStory, {
          foreignKey: 'UserID'
        });
      }
    }
  });
  return User;
};