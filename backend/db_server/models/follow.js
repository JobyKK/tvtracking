module.exports = (sequelize, DataTypes) => {
  const Follow = sequelize.define('Follow', {
    FollowID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    IsFollow: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    CRTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    UPTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    classMethods: {
      associate: (models) => {
        Follow.belongsTo(models.User, {
          foreignKey: 'FollowerID',
          as: 'Follower'
        });
        Follow.belongsTo(models.User, {
          foreignKey: 'PersonID',
          as: 'Person'
        });
      }
    }
  });
  return Follow;
};