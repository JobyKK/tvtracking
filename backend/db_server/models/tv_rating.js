module.exports = (sequelize, DataTypes) => {
  const TVRating = sequelize.define('TVRating', {
    TVRatingID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    TVShowID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    TVShowName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Rating: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CRTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    UPTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    classMethods: {
      associate: (models) => {
        TVRating.belongsTo(models.User, {
          foreignKey: 'UserID',
          as: 'User'
        });
      }
    }
  });
  return TVRating;
};